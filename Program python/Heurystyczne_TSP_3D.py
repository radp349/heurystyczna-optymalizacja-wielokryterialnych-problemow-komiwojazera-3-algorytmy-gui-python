import sys
from PyQt5.QtCore import QTimer, Qt
from PyQt5.QtWidgets import QApplication, QMainWindow, QLineEdit, QCheckBox, QComboBox, QFileDialog, QVBoxLayout, QButtonGroup, QRadioButton, QWidget, QPushButton, QHBoxLayout, QSpinBox, QDoubleSpinBox, QLabel, QSizePolicy
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
import matplotlib.animation as animation
import numpy as np
import random
import math as m
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.colors import Normalize
import os
from PIL import Image
import time
# 20, 50, 100, 150
class MyWindow(QMainWindow):
    
    class Ant:
        def __init__(self, start_point, start_point_id):
            self.visited_cities = [start_point]
            self.visited_cities_id = [start_point_id]
            self.unvisited_cities = []
            self.unvisited_cities_id = []
            self.current_city = start_point
            self.current_city_id = start_point_id
            self.full_route_length = m.inf
            
    def __init__(self):
        # Opcjonalny pomiar czasu:
        self.start_time = None
        self.paused_time = 0
        self.is_paused = False
        
        # Ustawienia Okna:
        super().__init__()
        self.setGeometry(200, 200, 1400, 700)
        self.setWindowTitle("Mgr Metaheurystyczne TSP 3D")
        self.delay = 1 #ms opóźnienia animacji
        self.spacingx = 10
        self.spacingy = 10
        self.numberx = 10
        self.numbery = 10
        self.z_range = [-100,100]
        self.Miasta = []
        
        self.fig = plt.figure(figsize=(16, 8))
        self.ax = self.fig.add_subplot(121, projection='3d')
        self.ax2 = self.fig.add_subplot(122)  
        self.ax2.set_xlabel('Epoka')
        self.ax2.set_ylabel('Długość trasy')
        
        plt.subplots_adjust(left=0.01, right=0.99, bottom=0.2, top=0.8, wspace=0.2)
        self.epochs = []
        self.path_lengths = []
        self.canvas = FigureCanvas(self.fig)
        self.canvas.setMinimumSize(400, 400)
        
        self.task2D = False
        self.rainbow3D = False
        
        self.cmap_colors = [(0.0, 1.0, 0.0),   # Zielony
                            (0.25, 1.0, 0.0),
                            (0.5, 1.0, 0.0),
                            (0.75, 1.0, 0.0),
                            (1.0, 1.0, 0.0),  # Żółty
                            (1.0, 0.84, 0.0),
                            (1.0, 0.67, 0.0),  # Pomarańczowy
                            (1.0, 0.51, 0.0),
                            (1.0, 0.33, 0.0),
                            (1.0, 0.16, 0.0),
                            (1.0, 0.0, 0.0)]  # Czerwony
        
        self.cmap = LinearSegmentedColormap.from_list('custom_cmap', self.cmap_colors, N=256)
        
        self.autosave = True
        self.autosave_period = 1000
        # Przechowywanie najlepszego rozwiązania
        self.best = None
        self.best_distance = m.inf
        self.average_distance = m.inf
        self.birth_of_the_best = 0
        self.epoch = 0
        self.epoch_info = None
        self.best_path_length_info = None


        # Ustawienia algorytmu genetycznego (+ wartości domyślne)
        self.population_size = 150
        self.offspring_size = 150
        self.holistic_succession = True
        self.Populacja = []
        self.Potomostwo = []
        self.Oceny = []
        self.crossover_chance = 0.75
        self.mutation_chance = 0.5
        
        # Ustawienia symulowanego wyżarzania:
        self.initial_temperature = self.temperature = 500
        self.cooling_rate = 0.003333
        self.new_sa = None
        self.current_sa = None
        self.risk_counter = 0
        
        # Ustawienia algorytmu mrówkowego:   
        self.Kolonia = []
        self.Prawdopodobienstwa = []
        self.pheromone_reset_threshold = 50 # Co ile grup mrówek bez poprawy rozwiązania resetujemy macierz feromonów
        self.local_ph_update = False # Dodajemy feromony na każdym odcinku na bieżąco bądź zbiorczo po wyznaczeniu całej trasy
        self.queen = False #Jeśli aktualizujemy feromon globalnie, to czy robimy to tylko dla najlepszej mrówki?
        self.num_ants = 20
        self.alpha = 1.0
        self.beta = 2.0
        self.rho = 0.5
        self.Q = 100.0
        self.pheromone_amount = None
        self.pheromone_matrix = None
        self.ant_distances = None
        
        # Menu przycisków:
        self.number_of_xcities_label = QLabel("Miasta Wzdłuż X:", self)  
        self.number_of_xcities_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.number_of_xcities_box = QSpinBox(self) 
        self.number_of_xcities_box.setValue(self.numberx)
        self.number_of_xcities_box.setMinimum(2)
        self.number_of_xcities_box.setMaximum(100)
        
        self.number_of_ycities_label = QLabel("Miasta Wszerz Y:", self)
        self.number_of_ycities_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.number_of_ycities_box = QSpinBox(self) 
        self.number_of_ycities_box.setValue(self.numbery)
        self.number_of_ycities_box.setMinimum(2)
        self.number_of_ycities_box.setMaximum(100)
        
        self.number_of_cities_label = QLabel("Łączna Liczba Miast:", self)  # Etykieta dla pola tekstowego
        self.number_of_cities_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.number_of_cities_box = QSpinBox(self) # Używamy QSpinBox do pobierania wartości liczbowych
        self.number_of_cities_box.setValue(100)
        self.number_of_cities_box.setMinimum(2)
        self.number_of_cities_box.setMaximum(1000)
        
        
        self.load_button = QRadioButton("Wczytaj Miasta", self)
        self.random_button = QRadioButton("Losuj Miasta", self)
        self.random_button.setChecked(True)  # Domyslnie wybieramy losowanie miast
        self.radio_group = QButtonGroup(self)
        self.radio_group.addButton(self.load_button)
        self.radio_group.addButton(self.random_button)
        self.load_button.setChecked(True)
        
        self.file_path = QLineEdit(self)
        self.file_path.setText('Zadania/star100_xyz.txt')
        
        self.zfunction_label = QLabel("Postać funkcji kosztu po Z:", self)  
        self.zfunction_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        
        self.zfunction_combo_box = QComboBox(self)
        self.zfunction_combo_box.addItem("Brak (Problem 2D)")
        self.zfunction_combo_box.addItem("Brak (Kryterium euklidesowe 3D)")
        self.zfunction_combo_box.addItem("Logarytm naturalny")
        self.zfunction_combo_box.addItem("Pierwiastek")
        self.zfunction_combo_box.addItem("Liniowa")
        self.zfunction_combo_box.addItem("Kwadrat")   
        self.zfunction_combo_box.addItem("Sześcian") 
        self.zfunction_combo_box.setCurrentIndex(4)  

        self.heuristic_label = QLabel("Wybór Algorytmu Optymalizacji:", self) 
        self.heuristic_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.genetic_button = QRadioButton("Genetyczny", self)
        self.genetic_button.clicked.connect(self.Ustawienia_Genetyczne)
        self.swarm_button = QRadioButton("Mrówkowy", self)
        self.swarm_button.clicked.connect(self.Ustawienia_Mrowkowego)
        self.annealing_button = QRadioButton("Symulowane Wyżarzanie", self)
        self.annealing_button.clicked.connect(self.Ustawienia_Wyzarzania)
        self.genetic_button.setChecked(True)  # Domyslnie wybieramy brak koewolucji
        self.heuristic_group = QButtonGroup(self)
        self.heuristic_group.addButton(self.genetic_button)
        self.heuristic_group.addButton(self.swarm_button)
        self.heuristic_group.addButton(self.annealing_button)
        
        self.nearest_neighbour_label = QLabel("Sposób Inicjalizacji Wstępnej:", self)  
        self.nearest_neighbour_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.random_init_button = QRadioButton("Inicjalizacja Losowa", self)
        self.nn_init_button = QRadioButton("Algorytm Najbliższego Sąsiada", self)
        self.random_init_button.setChecked(True) 
        self.nearest_neighbour_group = QButtonGroup(self)
        self.nearest_neighbour_group.addButton(self.random_init_button)
        self.nearest_neighbour_group.addButton(self.nn_init_button)
        
        self.selection_label = QLabel("Metoda selekcji:", self)  
        self.selection_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.roulette_only_button = QRadioButton("K. Ruletki", self)
        self.tournament_only_button = QRadioButton("Turniejowa", self)
        self.selection_switch_button = QRadioButton("Na Zmianę", self)
        self.selection_random_button = QRadioButton("Losowo", self)
        self.selection_switch_button.setChecked(True) 
        self.selection_group = QButtonGroup(self)
        self.selection_group.addButton(self.roulette_only_button)
        self.selection_group.addButton(self.tournament_only_button)
        self.selection_group.addButton(self.selection_switch_button)
        self.selection_group.addButton(self.selection_random_button)  
        
        self.succession_label = QLabel("Strategia sukcesji:", self)  
        self.succession_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.full_succession_button = QRadioButton("Sukcesja całościowa", self)
        self.partial_succession_button = QRadioButton("Sukcesja częściowa", self)
        self.partial_succession_button.setChecked(True) 
        self.succession_group = QButtonGroup(self)
        self.succession_group.addButton(self.full_succession_button)
        self.succession_group.addButton(self.partial_succession_button)

        self.population_size_label = QLabel("Rozmiar populacji:    ", self)  
        self.population_size_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.population_size_label.setFixedWidth(100)
        self.population_size_box = QSpinBox(self) 
        self.population_size_box.setMinimum(2)
        self.population_size_box.setMaximum(1000)
        self.population_size_box.setValue(self.population_size)
        
        self.offspring_size_label = QLabel("Rozmiar potomstwa:", self)  
        self.offspring_size_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.offspring_size_label.setFixedWidth(100)
        self.offspring_size_box = QSpinBox(self) 
        self.offspring_size_box.setMinimum(2)
        self.offspring_size_box.setMaximum(2000)
        self.offspring_size_box.setValue(self.offspring_size)

        self.operators_label = QLabel("Ustawienia Krzyżowania i Mutacji:", self)  
        self.operators_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        
        self.crossover_chance_label = QLabel("Prawdopodobieństwo Krzyżowania:", self)  
        self.crossover_chance_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.crossover_chance_label.setFixedWidth(170)
        self.crossover_chance_box = QDoubleSpinBox(self) 
        self.crossover_chance_box.setValue(self.crossover_chance)  
        self.crossover_chance_box.setMinimum(0) 
        self.crossover_chance_box.setMaximum(1)
        self.crossover_chance_box.setSingleStep(0.01) 
        
        self.cx_only_button = QRadioButton("Tylko CX", self)
        self.pmx_only_button = QRadioButton("Tylko PMX", self)
        self.crossover_switch_button = QRadioButton("Na Zmianę", self)
        self.crossover_random_button = QRadioButton("Losowo", self)
        self.crossover_switch_button.setChecked(True) 
        self.crossover_group = QButtonGroup(self)
        self.crossover_group.addButton(self.cx_only_button)
        self.crossover_group.addButton(self.pmx_only_button)
        self.crossover_group.addButton(self.crossover_switch_button)
        self.crossover_group.addButton(self.crossover_random_button)  
        
        self.mutation_chance_label = QLabel("Prawdopodobieństwo Mutacji:", self)  
        self.mutation_chance_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.mutation_chance_label.setFixedWidth(170)
        self.mutation_chance_box = QDoubleSpinBox(self) 
        self.mutation_chance_box.setValue(self.mutation_chance)  
        self.mutation_chance_box.setMinimum(0) 
        self.mutation_chance_box.setMaximum(1)
        self.mutation_chance_box.setSingleStep(0.01)
        
        self.mutation_combo_box = QComboBox(self)
        self.mutation_combo_box.addItem("Mutacja dostosowana do stopnia zaawansowania rozwiązania")
        self.mutation_combo_box.addItem("Wszystkie metody mutacji losowo")
        self.mutation_combo_box.addItem("Wszystkie metody mutacji na zmianę")
        self.mutation_combo_box.addItem("Tylko mutacja przez wymieszanie przedziału")
        self.mutation_combo_box.addItem("Tylko mutacja przez zamianę pary miast")   
        self.mutation_combo_box.addItem("Tylko mutacja przez inwersję i przesunięcie przedziału") 
        self.mutation_combo_box.addItem("Tylko mutacja przez przestawienie jednego miasta")    
        self.mutation_combo_box.addItem("Tylko mutacja przez inwersję przedziału")
        self.mutation_combo_box.setCurrentIndex(0)
        
        self.number_of_ants_label = QLabel("Liczba Mrówek:", self)  
        self.number_of_ants_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.number_of_ants_label.setHidden(True)
        self.number_of_ants_box = QSpinBox(self) 
        self.number_of_ants_box.setMinimum(1)
        self.number_of_ants_box.setMaximum(100000)
        self.number_of_ants_box.setValue(self.num_ants)
        self.number_of_ants_box.setHidden(True)
        
        self.pheromone_trail_label = QLabel("Sposób pozostawiania feromonów przez mrówkę:", self)  
        self.pheromone_trail_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.pheromone_trail_label.setHidden(True)
        self.local_pheromone_button = QRadioButton("Lokalnie na każdym odcinku", self)
        self.local_pheromone_button.clicked.connect(self.BlokujCheckbox)
        self.local_pheromone_button.setHidden(True)
        self.global_pheromone_button = QRadioButton("Globalnie na samym końcu", self)
        self.global_pheromone_button.clicked.connect(self.OdblokujCheckbox)
        self.global_pheromone_button.setHidden(True)
        
        self.local_pheromone_group = QButtonGroup(self)
        self.local_pheromone_group.addButton(self.local_pheromone_button)
        self.local_pheromone_group.addButton(self.global_pheromone_button)
        self.global_pheromone_button.setChecked(True)
        
        self.queen_checkbox = QCheckBox('Czy tylko najlepsza mrówka zostawia ślad feromonowy?', self)
        self.queen_checkbox.setHidden(True)
        
        self.pheromone_reset_threshold_label = QLabel("Próg Resetu Feromonów:", self)  
        self.pheromone_reset_threshold_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.pheromone_reset_threshold_label.setHidden(True)
        self.pheromone_reset_threshold_box = QSpinBox(self) 
        self.pheromone_reset_threshold_box.setMinimum(1)
        self.pheromone_reset_threshold_box.setMaximum(100000)
        self.pheromone_reset_threshold_box.setValue(self.pheromone_reset_threshold)
        self.pheromone_reset_threshold_box.setHidden(True)
        
        self.alpha_label = QLabel("Alfa:", self)
        self.alpha_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.alpha_label.setHidden(True)
        self.alpha_box = QDoubleSpinBox(self)
        self.alpha_box.setMinimum(0)
        self.alpha_box.setMaximum(100000)
        self.alpha_box.setDecimals(3)
        self.alpha_box.setSingleStep(0.001)
        self.alpha_box.setValue(self.alpha)
        self.alpha_box.setHidden(True)
        
        self.beta_label = QLabel("Beta:", self)
        self.beta_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.beta_label.setHidden(True)
        self.beta_box = QDoubleSpinBox(self)
        self.beta_box.setMinimum(0)
        self.beta_box.setMaximum(100000)
        self.beta_box.setDecimals(3)
        self.beta_box.setSingleStep(0.001)
        self.beta_box.setValue(self.beta)
        self.beta_box.setHidden(True)
        
        self.rho_label = QLabel("Rho:", self)
        self.rho_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.rho_label.setHidden(True)
        self.rho_box = QDoubleSpinBox(self)
        self.rho_box.setMinimum(0)
        self.rho_box.setMaximum(1)
        self.rho_box.setDecimals(3)
        self.rho_box.setSingleStep(0.001)
        self.rho_box.setValue(self.rho)
        self.rho_box.setHidden(True)
        
        self.q_label = QLabel(" Q:   ", self)
        self.q_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.q_label.setHidden(True)
        self.q_box = QDoubleSpinBox(self)
        self.q_box.setMinimum(0.001)
        self.q_box.setMaximum(100000)
        self.q_box.setDecimals(3)
        self.q_box.setSingleStep(0.001)
        self.q_box.setValue(self.Q)
        self.q_box.setHidden(True)
         
        self.initial_tepmerature_label = QLabel("Temperatura:", self)  
        self.initial_tepmerature_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.initial_tepmerature_label.setHidden(True)
        self.initial_tepmerature_box = QSpinBox(self) 
        self.initial_tepmerature_box.setMinimum(0)
        self.initial_tepmerature_box.setMaximum(1000000)
        self.initial_tepmerature_box.setValue(self.temperature)
        self.initial_tepmerature_box.setHidden(True)
        
        self.cooling_rate_label = QLabel("Tempo chłodzenia:", self)
        self.cooling_rate_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.cooling_rate_label.setHidden(True)
        self.cooling_rate_box = QDoubleSpinBox(self)
        self.cooling_rate_box.setMinimum(0)
        self.cooling_rate_box.setMaximum(1)
        self.cooling_rate_box.setDecimals(6)
        self.cooling_rate_box.setSingleStep(0.000001)
        self.cooling_rate_box.setValue(self.cooling_rate)
        self.cooling_rate_box.setHidden(True)
        
        self.sa_setting_label = QLabel("Ustawienia Symulowanego Wyzarzania:", self)
        self.sa_setting_label.setHidden(True)
        
        self.modification_combo_box = QComboBox(self)
        self.modification_combo_box.addItem("Metoda modyfikacji zależna od temperatury")
        self.modification_combo_box.addItem("Wszystkie metody modyfikacji losowo")
        self.modification_combo_box.addItem("Wszystkie metody modyfikacji na zmianę")
        self.modification_combo_box.addItem("Tylko modyfikacja przez wymieszanie przedziału")
        self.modification_combo_box.addItem("Tylko modyfikacja przez zamianę pary miast")   
        self.modification_combo_box.addItem("Tylko modyfikacja przez inwersję i przesunięcie przedziału") 
        self.modification_combo_box.addItem("Tylko modyfikacja przez przestawienie jednego miasta")    
        self.modification_combo_box.addItem("Tylko modyfikacja przez inwersję przedziału")
        self.modification_combo_box.setCurrentIndex(0)   
        self.modification_combo_box.setHidden(True)       
        
        self.iterations_label = QLabel("X =", self)  
        self.iterations_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.iterations_box = QSpinBox(self) 
        self.iterations_box.setValue(50)
        self.iterations_box.setMinimum(1)
        self.iterations_box.setMaximum(100)

        # Przyciski do rozpoczęcia, zatrzymania, wznowienia i zakończenia animacji
        self.start_button = QPushButton('Start', self)
        self.start_button.clicked.connect(self.Start_Animacji)
        self.start_button.setFixedWidth(325)
        self.browse_button = QPushButton("Przeglądaj...", self)
        self.browse_button.clicked.connect(self.Przegladaj_Pliki)
        self.stop_button = QPushButton('Zatrzymaj', self)
        self.stop_button.clicked.connect(self.Zatrzymanie_Animacji)
        self.stop_button.setEnabled(False)
        self.save_button = QPushButton('Zapisz Bieżące Rozwiązanie', self)
        self.save_button.clicked.connect(self.Zapis)
        self.save_button.setEnabled(False)
        self.autosave_checkbox = QCheckBox('Czy wykonywać autozapis co:', self)
        self.autosave_checkbox.setChecked(True)
        self.autosave_checkbox.clicked.connect(self.EdytujAutozapis)
        self.autosave_box = QSpinBox(self) 
        self.autosave_box.setMinimum(1)
        self.autosave_box.setMaximum(9999999)
        self.autosave_box.setValue(self.autosave_period)
        self.autosave_box.setEnabled(False)
        self.autosave_box_label = QLabel("epok?", self)
        self.resume_button = QPushButton('Wznów', self)
        self.resume_button.clicked.connect(self.Wznowienie_Animacji)
        self.resume_button.setEnabled(False)
        self.iterations_button = QPushButton('Wykonaj X iteracji', self)
        self.iterations_button.clicked.connect(self.X_iteracji)
        self.iterations_button.setEnabled(False)
        self.end_button = QPushButton('Zakończ', self)
        self.end_button.clicked.connect(self.Zakonczenie_Animacji)
        self.end_button.setEnabled(False)

        # Używamy QVBoxLayout dla przycisków
        self.button_layout = QVBoxLayout()
        self.button_layout.setSpacing(5)
        self.button_layout.setAlignment(Qt.AlignHCenter)
        
        self.number_of_xycities_layout = QHBoxLayout()
        self.number_of_xycities_layout.setSpacing(5)  
        self.number_of_xycities_layout.addWidget(self.number_of_xcities_label)
        self.number_of_xycities_layout.addWidget(self.number_of_xcities_box)
        self.number_of_xycities_layout.addWidget(self.number_of_ycities_label)
        self.number_of_xycities_layout.addWidget(self.number_of_ycities_box)
        
        self.number_of_cities_layout = QHBoxLayout()
        self.number_of_cities_layout.setSpacing(5)  
        self.number_of_cities_layout.addWidget(self.number_of_cities_label)
        self.number_of_cities_layout.addWidget(self.number_of_cities_box)
    
        
        self.radio_layout = QHBoxLayout()
        self.radio_layout.addWidget(self.load_button)
        self.radio_layout.addWidget(self.random_button)
        
        self.browse_layout = QHBoxLayout()
        self.browse_layout.addWidget(self.browse_button)
        self.browse_layout.addWidget(self.file_path)
        
        self.zfunction_layout = QHBoxLayout()
        self.zfunction_layout.addWidget(self.zfunction_label)
        self.zfunction_layout.addWidget(self.zfunction_combo_box)
        
        self.heuristic_center_label_layout = QHBoxLayout()
        self.heuristic_center_label_layout.addWidget(self.heuristic_label)
        
        
        self.heuristic_layout = QHBoxLayout()
        self.heuristic_layout.addWidget(self.genetic_button)
        self.heuristic_layout.addWidget(self.swarm_button)
        self.heuristic_layout.addWidget(self.annealing_button)

        
        self.nearest_neighbour_label_layout = QHBoxLayout()
        self.nearest_neighbour_label_layout.addWidget(self.nearest_neighbour_label)
        
        self.nearest_neighbour_layout = QHBoxLayout()
        self.nearest_neighbour_layout.addWidget(self.random_init_button)
        self.nearest_neighbour_layout.addWidget(self.nn_init_button)
        
        self.selection_label_layout = QHBoxLayout()
        self.selection_label_layout.addWidget(self.selection_label)
        
        self.selection_layout = QHBoxLayout()
        self.selection_layout.addWidget(self.roulette_only_button)
        self.selection_layout.addWidget(self.tournament_only_button)
        self.selection_layout.addWidget(self.selection_switch_button)
        self.selection_layout.addWidget(self.selection_random_button)  
             
        self.succession_label_layout = QHBoxLayout()
        self.succession_label_layout.addWidget(self.succession_label)
        
        self.succession_layout = QHBoxLayout()
        self.succession_layout.addWidget(self.full_succession_button)
        self.succession_layout.addWidget(self.partial_succession_button)
        
        self.population_size_layout = QHBoxLayout()
        self.population_size_layout.setSpacing(5)  
        self.population_size_layout.addWidget(self.population_size_label)
        self.population_size_layout.addWidget(self.population_size_box)
        
        self.offspring_size_layout = QHBoxLayout()
        self.offspring_size_layout.setSpacing(5)  
        self.offspring_size_layout.addWidget(self.offspring_size_label)
        self.offspring_size_layout.addWidget(self.offspring_size_box)
        
        self.operators_label_layout = QHBoxLayout()
        self.operators_label_layout.addWidget(self.operators_label)     
        
        self.crossover_chance_layout = QHBoxLayout()
        self.crossover_chance_layout.setSpacing(5)  
        self.crossover_chance_layout.addWidget(self.crossover_chance_label)
        self.crossover_chance_layout.addWidget(self.crossover_chance_box)
        
        self.crossover_layout = QHBoxLayout()
        self.crossover_layout.setSpacing(5)  
        self.crossover_layout.addWidget(self.cx_only_button)
        self.crossover_layout.addWidget(self.pmx_only_button)
        self.crossover_layout.addWidget(self.crossover_switch_button)
        self.crossover_layout.addWidget(self.crossover_random_button)
                
        self.mutation_chance_layout = QHBoxLayout()
        self.mutation_chance_layout.setSpacing(5)  
        self.mutation_chance_layout.addWidget(self.mutation_chance_label)
        self.mutation_chance_layout.addWidget(self.mutation_chance_box)
        
        self.mutation_layout = QHBoxLayout()
        self.mutation_layout.addWidget(self.mutation_combo_box)
        
        self.sa_setting_label_layout = QHBoxLayout()
        self.sa_setting_label_layout.addWidget(self.sa_setting_label)
        self.sa_setting_label_layout.setAlignment(Qt.AlignHCenter)
        
        self.number_of_ants_layout = QHBoxLayout()
        self.number_of_ants_layout.setSpacing(5)  
        self.number_of_ants_layout.addWidget(self.number_of_ants_label)
        self.number_of_ants_layout.addWidget(self.number_of_ants_box)
        
        self.pheromone_trail_label_layout = QHBoxLayout()
        self.pheromone_trail_label_layout.setSpacing(5)  
        self.pheromone_trail_label_layout.addWidget(self.pheromone_trail_label)
        
        self.pheromone_trail_layout = QHBoxLayout()
        self.pheromone_trail_layout.setSpacing(5)  
        self.pheromone_trail_layout.addWidget(self.local_pheromone_button)
        self.pheromone_trail_layout.addWidget(self.global_pheromone_button)
        
        self.queen_checkbox_layout = QHBoxLayout()
        self.queen_checkbox_layout.setSpacing(5)
        self.queen_checkbox_layout.addWidget(self.queen_checkbox)
        self.queen_checkbox_layout.setAlignment(Qt.AlignHCenter)
        
        self.autosave_checkbox_layout = QHBoxLayout()
        self.autosave_checkbox_layout.setSpacing(5)
        self.autosave_checkbox_layout.addWidget(self.autosave_checkbox)
        self.autosave_checkbox_layout.addWidget(self.autosave_box)
        self.autosave_checkbox_layout.addWidget(self.autosave_box_label)
        self.autosave_checkbox_layout.setAlignment(Qt.AlignHCenter)

        self.alpa_beta_layout = QHBoxLayout()
        self.alpa_beta_layout.setSpacing(5)  
        self.alpa_beta_layout.addWidget(self.alpha_label)
        self.alpa_beta_layout.addWidget(self.alpha_box)
        self.alpa_beta_layout.addWidget(self.beta_label)
        self.alpa_beta_layout.addWidget(self.beta_box)
        
        self.rho_q_layout = QHBoxLayout()
        self.rho_q_layout.setSpacing(5)  
        self.rho_q_layout.addWidget(self.rho_label)
        self.rho_q_layout.addWidget(self.rho_box)
        self.rho_q_layout.addWidget(self.q_label)
        self.rho_q_layout.addWidget(self.q_box)
        
        self.pheromone_reset_threshold_layout = QHBoxLayout()
        self.pheromone_reset_threshold_layout.setSpacing(5)  
        self.pheromone_reset_threshold_layout.addWidget(self.pheromone_reset_threshold_label)
        self.pheromone_reset_threshold_layout.addWidget(self.pheromone_reset_threshold_box)
        
        self.temperature_layout = QHBoxLayout()
        self.temperature_layout.setSpacing(5)  
        self.temperature_layout.addWidget(self.initial_tepmerature_label)
        self.temperature_layout.addWidget(self.initial_tepmerature_box)
        self.temperature_layout.addWidget(self.cooling_rate_label)
        self.temperature_layout.addWidget(self.cooling_rate_box)
        
        self.modification_layout = QHBoxLayout()
        self.modification_layout.addWidget(self.modification_combo_box)
              
        self.iterations_layout = QHBoxLayout()
        self.iterations_layout.setContentsMargins(0, 5, 0, 0)
        self.iterations_layout.addWidget(self.iterations_button)
        self.iterations_layout.addWidget(self.iterations_label)
        self.iterations_layout.addWidget(self.iterations_box)
        
        self.button_layout.addWidget(self.start_button)
        self.button_layout.addLayout(self.number_of_xycities_layout)
        self.button_layout.addLayout(self.number_of_cities_layout)
        self.button_layout.addLayout(self.radio_layout)
        self.button_layout.addLayout(self.browse_layout)
        self.button_layout.addLayout(self.zfunction_layout)
        self.button_layout.addLayout(self.heuristic_center_label_layout)
        self.button_layout.addLayout(self.heuristic_layout)
        self.button_layout.addLayout(self.nearest_neighbour_label_layout)
        self.button_layout.addLayout(self.nearest_neighbour_layout)
        
        # Ustawienia Genetyczne
        self.button_layout.addLayout(self.selection_label_layout)
        self.button_layout.addLayout(self.selection_layout)        
        self.button_layout.addLayout(self.succession_label_layout)
        self.button_layout.addLayout(self.succession_layout)
        self.button_layout.addLayout(self.population_size_layout)
        self.button_layout.addLayout(self.offspring_size_layout)
        self.button_layout.addLayout(self.operators_label_layout)
        self.button_layout.addLayout(self.crossover_chance_layout)
        self.button_layout.addLayout(self.crossover_layout)
        self.button_layout.addLayout(self.mutation_chance_layout)
        self.button_layout.addLayout(self.mutation_layout)
        
        #Ustawienia Mrówkowego
        self.button_layout.addLayout(self.number_of_ants_layout)
        self.button_layout.addLayout(self.pheromone_trail_label_layout)
        self.button_layout.addLayout(self.pheromone_trail_layout)
        self.button_layout.addLayout(self.queen_checkbox_layout)
        self.button_layout.addLayout(self.alpa_beta_layout)
        self.button_layout.addLayout(self.rho_q_layout)
        self.button_layout.addLayout(self.pheromone_reset_threshold_layout)        
        
        #Ustawienia Symulowanego Wyżarzania
        self.button_layout.addLayout(self.sa_setting_label_layout)
        self.button_layout.addLayout(self.temperature_layout)
        self.button_layout.addLayout(self.modification_layout)
        
        self.button_layout.addLayout(self.iterations_layout)
        self.button_layout.addWidget(self.stop_button)
        self.button_layout.addLayout(self.autosave_checkbox_layout)
        self.button_layout.addWidget(self.save_button)
        self.button_layout.addWidget(self.resume_button)
        self.button_layout.addWidget(self.end_button)
        self.button_layout.addStretch(1)
        
        self.layout = QHBoxLayout()
        self.layout.addLayout(self.button_layout) # Dodajemy przyciski do layoutu
        self.layout.addWidget(self.canvas)

        self.central_widget = QWidget()
        self.central_widget.setLayout(self.layout)
        self.setCentralWidget(self.central_widget)

        self.animation = None

        self.Punkty = []
        self.Linie = []

        self.licznik = QTimer()
        self.licznik.timeout.connect(self.canvas.draw)

    def Ustawienia_Genetyczne(self):
        genetic_elements_to_display = [
            self.nearest_neighbour_label, self.random_init_button, self.nn_init_button,
            self.selection_label, self.roulette_only_button, self.tournament_only_button,
            self.selection_switch_button, self.selection_random_button, self.succession_label,
            self.full_succession_button, self.partial_succession_button,
            self.population_size_box, self.population_size_label,
            self.offspring_size_box, self.offspring_size_label, 
            self.operators_label, self.crossover_chance_box, self.crossover_chance_label,
            self.mutation_chance_box, self.mutation_chance_label, self.mutation_combo_box,
            self.cx_only_button, self.pmx_only_button,
            self.crossover_switch_button, self.crossover_random_button
        ]

        for element in genetic_elements_to_display:
            element.setHidden(False)
            
        genetic_elements_to_hide = [
            self.number_of_ants_label, self.number_of_ants_box,
            self.pheromone_trail_label, self.global_pheromone_button, self.local_pheromone_button,
            self.alpha_label, self.alpha_box, self.beta_label, self.beta_box,
            self.rho_label, self.rho_box, self.q_label, self.q_box,
            self.pheromone_reset_threshold_label, self.pheromone_reset_threshold_box, self.queen_checkbox,
            self.sa_setting_label, self.initial_tepmerature_label, self.initial_tepmerature_box,
            self.cooling_rate_label, self.cooling_rate_box ,self.modification_combo_box
        ]

        for element in genetic_elements_to_hide:
            element.setHidden(True)

    def Ustawienia_Mrowkowego(self):
        swarm_elements_to_display = [
            self.number_of_ants_label, self.number_of_ants_box, 
            self.pheromone_trail_label, self.global_pheromone_button, self.local_pheromone_button,
            self.queen_checkbox, self.alpha_label, self.alpha_box, self.beta_label, self.beta_box,
            self.rho_label, self.rho_box, self.q_label, self.q_box,
            self.pheromone_reset_threshold_label, self.pheromone_reset_threshold_box
        ]

        for element in swarm_elements_to_display:
            element.setHidden(False)
            
        swarm_elements_to_hide = [
            self.nearest_neighbour_label, self.random_init_button, self.nn_init_button,
            self.selection_label, self.roulette_only_button, self.tournament_only_button,
            self.selection_switch_button, self.selection_random_button, self.succession_label,
            self.full_succession_button, self.partial_succession_button,
            self.population_size_box, self.population_size_label,
            self.offspring_size_box, self.offspring_size_label, 
            self.operators_label, self.crossover_chance_box, self.crossover_chance_label,
            self.mutation_chance_box, self.mutation_chance_label, self.mutation_combo_box,
            self.cx_only_button, self.pmx_only_button,
            self.crossover_switch_button, self.crossover_random_button, self.sa_setting_label,
            self.initial_tepmerature_label, self.initial_tepmerature_box, self.cooling_rate_label, 
            self.cooling_rate_box, self.modification_combo_box
        ]

        for element in swarm_elements_to_hide:
            element.setHidden(True)
      
    def Ustawienia_Wyzarzania(self):
        annaeling_elements_to_display = [
        self.nearest_neighbour_label, self.random_init_button, self.nn_init_button,
        self.sa_setting_label, self.initial_tepmerature_label, self.initial_tepmerature_box,
        self.cooling_rate_label, self.cooling_rate_box ,self.modification_combo_box
        ]

        for element in annaeling_elements_to_display:
            element.setHidden(False)
            
        annaeling_elements_to_hide = [
            self.selection_label, self.roulette_only_button, self.tournament_only_button,
            self.selection_switch_button, self.selection_random_button, self.succession_label,
            self.full_succession_button, self.partial_succession_button,
            self.population_size_box, self.population_size_label,
            self.offspring_size_box, self.offspring_size_label, 
            self.operators_label, self.crossover_chance_box, self.crossover_chance_label,
            self.mutation_chance_box, self.mutation_chance_label, self.mutation_combo_box,
            self.cx_only_button, self.pmx_only_button,
            self.crossover_switch_button, self.crossover_random_button,
            self.number_of_ants_label, self.number_of_ants_box,
            self.pheromone_trail_label, self.global_pheromone_button, self.local_pheromone_button,
            self.queen_checkbox, self.alpha_label, self.alpha_box, self.beta_label, self.beta_box,
            self.rho_label, self.rho_box, self.q_label, self.q_box,
            self.pheromone_reset_threshold_label, self.pheromone_reset_threshold_box
        ]

        for element in annaeling_elements_to_hide:
            element.setHidden(True)
            
    def Dez_Aktywacja(self):
        elements_to_disable = [
            self.number_of_xcities_box, self.number_of_ycities_box, self.number_of_cities_box, 
            self.load_button, self.random_button, self.zfunction_combo_box,
            self.start_button, self.genetic_button, self.swarm_button,
            self.annealing_button, self.random_init_button, self.nn_init_button,
            self.roulette_only_button, self.tournament_only_button,
            self.selection_switch_button, self.selection_random_button,
            self.full_succession_button, self.partial_succession_button,
            self.population_size_box, self.offspring_size_box,
            self.crossover_chance_box, self.mutation_chance_box,
            self.cx_only_button, self.pmx_only_button, self.crossover_switch_button, self.crossover_random_button,
            self.mutation_combo_box, self.number_of_ants_box, self.global_pheromone_button, self.local_pheromone_button, 
            self.queen_checkbox, self.alpha_box, self.beta_box, self.rho_box, 
            self.q_box,self.pheromone_reset_threshold_box, self.initial_tepmerature_box, self.cooling_rate_box, self.modification_combo_box
        ]
            
        for element in elements_to_disable:
            element.setEnabled(False)
            
        elements_to_enable = [
            self.iterations_button, self.stop_button, self.save_button, self.end_button
        ]

        for element in elements_to_enable:
            element.setEnabled(True)
            
    def BlokujCheckbox(self):
        self.queen_checkbox.setEnabled(False)
        self.queen_checkbox.setChecked(False)
        
    def OdblokujCheckbox(self):
        self.queen_checkbox.setEnabled(True)
        
    def EdytujAutozapis(self):
        self.autosave = self.autosave_checkbox.isChecked()
        self.autosave_period = self.autosave_box.value()
        self.autosave_box.setEnabled(not (self.autosave_checkbox.isChecked()))
              
    # Metody do rozpoczęcia, zatrzymania, wznowienia i zakończenia animacji
    def Start_Animacji(self):
        self.Dez_Aktywacja()
        if not self.animation:
            self.task2D = self.zfunction_combo_box.itemText(self.zfunction_combo_box.currentIndex()) == "Brak (Problem 2D)"
            self.rainbow3D = self.zfunction_combo_box.itemText(self.zfunction_combo_box.currentIndex()) == "Brak (Kryterium euklidesowe 3D)"
            

            if not (self.task2D or self.rainbow3D) :
                self.cmap = LinearSegmentedColormap.from_list('custom_cmap', self.cmap_colors, N=256)
            self.Miasta.clear()
            if self.random_button.isChecked():  # Jeżeli wybrano losowanie miast
                self.Losuj_Miasta()
            elif self.load_button.isChecked():  # Jeżeli wybrano wczytywanie miast
                self.Wczytaj_Miasta()
                               
            self.best = self.Miasta.copy()
            self.best_distance = sum(self.Dystans(self.best[k-1],self.best[k]) for k in range(len(self.best)))
            self.birth_of_the_best = 0
                
            if self.genetic_button.isChecked():
                self.population_size = self.population_size_box.value()
                if self.full_succession_button.isChecked():  
                    self.holistic_succession = True
                    self.offspring_size = self.population_size_box.value()
                elif self.partial_succession_button.isChecked():
                    self.holistic_succession = False
                    self.offspring_size = self.offspring_size_box.value()     
                self.crossover_chance = self.crossover_chance_box.value()
                self.mutation_chance = self.mutation_chance_box.value()    
                
                self.Inicjalizacja()
                self.Przystosowanie()
                self.Sortuj_Populacje()
                self.animation = animation.FuncAnimation(self.fig, lambda frame_number: self.Main_Genetyczny(), init_func=self.Rysuj_Genetyczny, frames=999999, interval=self.delay, blit=False)
            
            elif self.swarm_button.isChecked():
                self.pheromone_reset_threshold = self.pheromone_reset_threshold_box.value()
                self.local_ph_update = self.local_pheromone_button.isChecked()
                self.queen = self.queen_checkbox.isChecked()
                self.num_ants = self.number_of_ants_box.value()
                self.alpha = self.alpha_box.value()
                self.beta = self.beta_box.value()
                self.rho = self.rho_box.value()
                self.Q = self.q_box.value()
                self.pheromone_amount = 1/self.cities_number
                self.Reset_Feromonowy()
                self.ant_distances = np.zeros((self.cities_number,self.cities_number))
                for i in range(self.cities_number):
                    for j in range(i+1, self.cities_number): 
                        self.ant_distances[i][j] = self.ant_distances[j][i]= self.Dystans(self.Miasta[i], self.Miasta[j])
                self.animation = animation.FuncAnimation(self.fig, lambda frame_number: self.Main_Mrowkowy(), init_func=self.Rysuj_Mrowkowy, frames=999999, interval=self.delay, blit=False)
            
            elif self.annealing_button.isChecked():
                self.temperature = self.initial_temperature = self.initial_tepmerature_box.value()
                self.cooling_rate = self.cooling_rate_box.value()
                if self.nn_init_button.isChecked():
                    self.Algorytm_Najblizszego_Sasiada_SA()
                self.current_sa = self.Miasta.copy()
                self.current_sa_distance = sum(self.Dystans(self.current_sa[p-1],self.current_sa[p]) for p in range(len(self.current_sa)))
                self.new_sa = self.Miasta.copy()
                self.risk_counter = 0
                
                self.animation = animation.FuncAnimation(self.fig, lambda frame_number: self.Main_Wyzarzanie(), init_func=self.Rysuj_Wyzarzanie, frames=999999, interval=self.delay, blit=False)
            
            if self.zfunction_combo_box.itemText(self.zfunction_combo_box.currentIndex()) == "Brak (Problem 2D)":
                self.ax.view_init(elev=90, azim=0)
           
            self.start_time = time.time()
            self.is_paused = False
            self.paused_time = 0
            self.licznik.start(1)
            self.start_button.setEnabled(False) # Blokujemy przycisk Start

    def Zatrzymanie_Animacji(self):
        if self.animation:
            self.animation.event_source.stop()
            self.stop_button.setEnabled(False)
            self.resume_button.setEnabled(True)
            if not self.is_paused:
                self.paused_time += time.time() - self.start_time
                self.is_paused = True

    def Wznowienie_Animacji(self):
        if self.animation:
            self.animation.event_source.start()
            self.stop_button.setEnabled(True)
            self.resume_button.setEnabled(False)
            if self.is_paused:
                self.start_time = time.time() - self.paused_time
                self.is_paused = False

    def Zakonczenie_Animacji(self):
        if self.animation:
            self.animation.event_source.stop()
            self.animation = None
            
            elapsed_time = time.time() - self.start_time
            # print(f"Czas upłynął: {elapsed_time:.2f} sekund")
            
            hours = int(round(elapsed_time // 3600,2))
            minutes = int(round((elapsed_time % 3600) // 60,2))
            seconds = int(round(elapsed_time % 60,2))

            print(f"Upłynęło {hours} godzin {minutes} minut i {seconds} sekund(y)")
                          
            elements_to_enable = [
            self.number_of_xcities_box, self.number_of_ycities_box, self.number_of_cities_box, self.load_button,
            self.random_button, self.start_button, self.zfunction_combo_box, self.genetic_button,
            self.swarm_button, self.annealing_button, self.random_init_button, self.nn_init_button,
            self.roulette_only_button, self.tournament_only_button, self.selection_switch_button,
            self.selection_random_button, self.full_succession_button, self.partial_succession_button,
            self.population_size_box, self.offspring_size_box, self.crossover_chance_box, self.mutation_combo_box,
            self.mutation_chance_box, self.cx_only_button, self.pmx_only_button, self.crossover_switch_button, 
            self.crossover_random_button, self.number_of_ants_box, self.global_pheromone_button, 
            self.local_pheromone_button,self.queen_checkbox, self.alpha_box, self.beta_box, self.rho_box, 
            self.q_box, self.pheromone_reset_threshold_box, self.initial_tepmerature_box, self.cooling_rate_box, self.modification_combo_box
            ]

            for element in elements_to_enable:
                element.setEnabled(True)
            
            elements_to_disable = [
                self.stop_button, self.save_button, self.iterations_button, self.resume_button, self.end_button
            ]
                
            for element in elements_to_disable:
                element.setEnabled(False)
            
            self.epoch = 0

            # Usuwamy punkty i linie z wykresu
            self.Punkty = []
            self.Linie = []

            # Czyścimy wykres i wymuszamy jego przerysowanie
            self.ax.cla()
            self.ax2.cla()
            self.ax2.set_xlabel('Epoka')
            self.ax2.set_ylabel('Długość trasy')
            self.canvas.draw()
            

    def Przegladaj_Pliki(self):
        # Otwieramy okno dialogowe do wyboru pliku
        file_name, _ = QFileDialog.getOpenFileName(self, "Select file", "", "Text Files (*.txt)")
        
        # Jeżeli użytkownik wybrał jakiś plik, ustawiamy ścieżkę do tego pliku w polu do wpisania ścieżki
        if file_name:
            self.file_path.setText(file_name)
            
    
        
    def Wczytaj_Miasta(self):
        self.Miasta.clear() 
        with open(self.file_path.text()) as data:
            for line in data:
                cell = line.split()
                cell[0] = float(cell[0])
                cell[1] = float(cell[1])
                if self.task2D:
                    cell[2] = float(0)
                else:
                    cell[2] = float(cell[2])
                self.Miasta.append(cell)
        self.cities_number = len(self.Miasta)

                
    def Losuj_Miasta(self):
        self.Miasta.clear()
        self.numberx = self.number_of_xcities_box.value()
        self.numbery = self.number_of_ycities_box.value()
        
        self.ax.set_xlim(0, (self.numberx - 1)*self.spacingx)
        self.ax.set_ylim(0, (self.numbery - 1)*self.spacingy)
        self.ax.set_zlim(self.z_range[0], self.z_range[1])
        
        self.cities_number = min(self.number_of_cities_box.value(),self.numberx*self.numbery)
        
        self.cities_grid = [(x, y) for x in range(self.numberx) for y in range(self.numbery)]
        random.shuffle(self.cities_grid)

        if self.task2D:
            for x, y in self.cities_grid[:self.cities_number]:
                self.Miasta.append([x*self.spacingx, y*self.spacingy, 0])
                # Wersja dla rozkładu normalnego
                #self.Miasta.append([ x*self.spacingx , y*self.spacingy , round(random.gauss((self.z_range[0]+self.z_range[1])/2,(abs(self.z_range[0]-self.z_range[1]))/6), 6)] )
        else:
            for x, y in self.cities_grid[:self.cities_number]:
                self.Miasta.append([x*self.spacingx, y*self.spacingy, round(random.uniform(self.z_range[0], self.z_range[1]), 6)])
                # Wersja dla rozkładu normalnego
                #self.Miasta.append([ x*self.spacingx , y*self.spacingy , round(random.gauss((self.z_range[0]+self.z_range[1])/2,(abs(self.z_range[0]-self.z_range[1]))/6), 6)] )
          


    def Dystans(self, city1, city2):
        x1, y1, z1 = city1
        x2, y2, z2 = city2
        
        expression = self.zfunction_combo_box.itemText(self.zfunction_combo_box.currentIndex())
        if expression == "Brak (Problem 2D)":
            return m.sqrt((x2-x1)**2 + (y2-y1)**2)
        elif expression == "Brak (Kryterium euklidesowe 3D)":
            return m.sqrt((x2-x1)**2 + (y2-y1)**2 + (z2-z1)**2)
        elif expression == "Logarytm naturalny":
            if abs(z2-z1) >= 1:
                return m.sqrt((x2-x1)**2 + (y2-y1)**2) + m.log(abs(z2-z1))
            else:
                return m.sqrt((x2-x1)**2 + (y2-y1)**2)
        elif expression == "Pierwiastek":
            return m.sqrt((x2-x1)**2 + (y2-y1)**2) + abs(z2-z1)**0.5
        elif expression == "Liniowa":
            return m.sqrt((x2-x1)**2 + (y2-y1)**2) + abs(z2-z1)
        elif expression == "Kwadrat":
            return m.sqrt((x2-x1)**2 + (y2-y1)**2) + (z2-z1)**2
        elif expression == "Sześcian":
            return m.sqrt((x2-x1)**2 + (y2-y1)**2) + abs(z2-z1)**3
        return 0
              
    def Inicjalizacja(self):
        self.Populacja = []
        for i in range(self.population_size):
            temp = self.Miasta.copy()
            if i > 0:
                random.shuffle(temp)
            self.Populacja.append(temp)
        if self.nn_init_button.isChecked():
            self.Algorytm_Najblizszego_Sasiada_Genetyczny()
            
    def Algorytm_Najblizszego_Sasiada_Genetyczny(self):
            for i in range (len(self.Populacja)):
        
                if len(self.Populacja[i]) > 3:   
                    cities_to_visit = self.Populacja[i].copy()
                    self.Populacja[i].clear()

                    index_startowy = random.randint(0, len(cities_to_visit)-1)
                    current_city = cities_to_visit[index_startowy]
                    self.Populacja[i].append(current_city)
                    cities_to_visit.remove(current_city)

                    while len(cities_to_visit) > 0:
                        distances = [(city, m.sqrt((self.Populacja[i][-1][0] - city[0]) ** 2 + (self.Populacja[i][-1][1] - city[1]) ** 2 + (self.Populacja[i][-1][2] - city[2]) ** 2)) for city in cities_to_visit]
                        nearest_city, _ = min(distances, key=lambda item:item[1])
                        self.Populacja[i].append(nearest_city)
                        cities_to_visit.remove(nearest_city)
                        
    def Algorytm_Najblizszego_Sasiada_SA(self):
 
        cities_to_visit = self.Miasta.copy()
        self.Miasta.clear()

        index_startowy = random.randint(0, len(cities_to_visit)-1)
        current_city = cities_to_visit[index_startowy]
        self.Miasta.append(current_city)
        cities_to_visit.remove(current_city)

        while len(cities_to_visit) > 0:
            distances = [(city, m.sqrt((self.Miasta[-1][0] - city[0]) ** 2 + (self.Miasta[-1][1] - city[1]) ** 2 + (self.Miasta[-1][2] - city[2]) ** 2)) for city in cities_to_visit]
            nearest_city, _ = min(distances, key=lambda item:item[1])
            self.Miasta.append(nearest_city)
            cities_to_visit.remove(nearest_city)
                        
          
    def Przystosowanie(self):
        self.Oceny = [] 
        for o in range(len(self.Populacja)):
            ocena = sum(self.Dystans(self.Populacja[o][k-1],self.Populacja[o][k]) for k in range(len(self.Populacja[o])))          
            # ocena = sum(m.sqrt((self.Populacja[o][k-1][0]-self.Populacja[o][k][0])**2 + (self.Populacja[o][k-1][1]-self.Populacja[o][k][1])**2 + (self.Populacja[o][k-1][2]-self.Populacja[o][k][2])**2) for k in range(len(self.Populacja[o])))
            self.Oceny.append(ocena)


    def Sortuj_Populacje(self):
        indices = np.argsort(self.Oceny)
        Posortowana_Populacja = [self.Populacja[i] for i in indices]
        Posortowane_Oceny = [self.Oceny[i] for i in indices]

        self.Populacja.clear()
        self.Populacja.extend(Posortowana_Populacja)

        self.Oceny.clear()
        self.Oceny.extend(Posortowane_Oceny)
            
    def Ruletka_rankingowa(self):
        summary = sum(range(0,len(self.Populacja)+1))
        wages = list(range(len(self.Populacja))+np.ones(len(self.Populacja)))
        wages.sort(reverse=True)
        
        self.Sortuj_Populacje()
        Szanse=[]
        
        for k in wages:
            chance = (k/summary)*100
            if k != wages[0]:
                chance+=Szanse[len(Szanse)-1]
            Szanse.append(chance)
            
        Los = []
        for _ in range(self.offspring_size):
            ticket = random.uniform(0, 100)
            Los.append(ticket)
            
        for ticket in Los:
            index = 0
            while(ticket>Szanse[index]):
                index+=1
        
            self.Potomostwo.append(self.Populacja[index])
        
        
    def Turniejowa(self):
        for i in range(self.offspring_size):
            skip = 0
            while(i >= len(self.Populacja)):
                i -= len(self.Populacja)
                skip += 1
            
            Turniej = []
            Noty =[]
            
            Turniej.append(self.Populacja[i-1-skip])
            Turniej.append(self.Populacja[i])
            Noty.append(self.Oceny[i-1-skip])
            Noty.append(self.Oceny[i])
                
            lists = random.uniform(0, 1)
            if 1 >= lists:
                self.Potomostwo.append(Turniej[Noty.index(min(Noty))])
            else:
                self.Potomostwo.append(Turniej[Noty.index(max(Noty))])
    
    def Selekcja(self):
        self.Potomostwo = []
        if self.roulette_only_button.isChecked():
            self.Ruletka_rankingowa()
        elif self.tournament_only_button.isChecked():
            self.Turniejowa()
        elif self.selection_switch_button.isChecked():
            if self.epoch % 2 == 0:
                self.Ruletka_rankingowa()
            elif self.epoch % 2 == 1:
                self.Turniejowa()
        elif self.selection_random_button.isChecked():
            x = random.randint(0, 1)
            if x==0:
                self.Ruletka_rankingowa()
            else:
                self.Turniejowa()

    def Krzyzowanie_PMX(self):
        if len(self.Potomostwo[0]) > 3:
            for i in range(int(len(self.Potomostwo)/2)):
                if random.uniform(0, 1) <= self.crossover_chance:
                    parent1 = self.Potomostwo[2*i]
                    parent2 = self.Potomostwo[(2*i)+1]

                    # Losujemy dwa punkty graniczne krzyżowania
                    cutpoint1 = random.randint(0, len(parent1) - 1)
                    cutpoint2 = random.randint(cutpoint1 + 1, len(parent1))

                    # Tworzymy kopie rodziców, aby nie zmieniać ich podczas krzyżowania
                    child1 = parent1[:]
                    child2 = parent2[:]

                    # Zamieniamy fragmenty między punktami granicznymi
                    child1[cutpoint1:cutpoint2] = parent2[cutpoint1:cutpoint2]
                    child2[cutpoint1:cutpoint2] = parent1[cutpoint1:cutpoint2]

                    # Naprawiamy powtarzające się wartości w potomkach
                    for k in range(len(child1)):
                        if k < cutpoint1 or k >= cutpoint2:
                            while child1[k] in child1[cutpoint1:cutpoint2]:
                                index = child1[cutpoint1:cutpoint2].index(child1[k])
                                child1[k] = parent1[cutpoint1:cutpoint2][index]

                        if k < cutpoint1 or k >= cutpoint2:
                            while child2[k] in child2[cutpoint1:cutpoint2]:
                                index = child2[cutpoint1:cutpoint2].index(child2[k])
                                child2[k] = parent2[cutpoint1:cutpoint2][index]
                            
                    self.Potomostwo[2*i] = child1.copy()
                    self.Potomostwo[2*i+1] = child2.copy()

    def Krzyzowanie_CX(self):
        if len(self.Potomostwo[0]) > 3:
            for i in range(int(len(self.Potomostwo)/2)):
                if random.uniform(0, 1) <= self.crossover_chance:
                    parent1 = self.Potomostwo[2*i]
                    parent2 = self.Potomostwo[(2*i)+1]

                    n = len(parent1)
                    child1 = [None] * n
                    child2 = [None] * n

                    # Inicjalizacja tablic, które śledzą indeksy odwiedzonych elementów
                    visited1 = [False] * n
                    visited2 = [False] * n

                    # Rozpoczęcie cyklu krzyżowania CX

                    cycle_start = random.randint(0, n-1)

                    while True:
                        # Dodajemy elementy do potomków z pierwszego rodzica
                        child1[cycle_start] = parent1[cycle_start]
                        child2[cycle_start] = parent2[cycle_start]
                        visited1[cycle_start] = True
                        visited2[cycle_start] = True

                        # Wyszukujemy index elementu w drugim rodzicu
                        index_in_parent2 = parent2.index(parent1[cycle_start])

                        # Przejście do kolejnego cyklu, jeśli index już był odwiedzony
                        if visited1[index_in_parent2]:
                            break
                        else:
                            cycle_start = index_in_parent2

                    # Wypełnienie brakujących elementów potomków
                    for k in range(n):
                        if child1[k] is None:
                            child1[k] = parent2[k]
                        if child2[k] is None:
                            child2[k] = parent1[k]
                            
                    self.Potomostwo[2*i] = child1.copy()
                    self.Potomostwo[2*i+1] = child2.copy()
                        
    def Krzyzowanie(self):
        if self.cx_only_button.isChecked():
            self.Krzyzowanie_CX()
        elif self.pmx_only_button.isChecked():
            self.Krzyzowanie_PMX()
        elif self.crossover_switch_button.isChecked():
            if self.epoch % 2 == 0:
                self.Krzyzowanie_CX()
            elif self.epoch % 2 == 1:
                self.Krzyzowanie_PMX()
        elif self.crossover_random_button.isChecked():
            x = random.randint(0, 1)
            if x==0:
                self.Krzyzowanie_CX()
            else:
                self.Krzyzowanie_PMX()
            
        
    def Mutacja(self):
        if len(self.Potomostwo[0]) > 3:
            for i in range(len(self.Potomostwo)):
                if random.uniform(0, 1) <= self.mutation_chance:

                    index1, index2 = random.sample(range(0, (len(self.Potomostwo[i])-1)), 2)

                        
                    if index1 > index2:
                        index1, index2 = index2, index1

                    if self.mutation_combo_box.itemText(self.mutation_combo_box.currentIndex()) == "Mutacja dostosowana do stopnia zaawansowania rozwiązania":   
                        milestone1 = int(self.cities_number*self.population_size/30) # Do tego momentu mieszanie przedziału
                        milestone2 = int(self.cities_number*self.population_size/8) # Do tego momentu zamiana pary i przesuwanie przedziału
                            

                        if self.epoch < milestone1:
                            x = random.randint(0, 4)
                            if x > 2:
                                x = 0
                        elif self.epoch < milestone2:
                            x = random.randint(1, 10)
                            if x > 2 and x % 2 == 1:
                                x = 1
                            elif x > 2 and x % 2 == 0:
                                x = 2
                        else:
                            x = random.randint(2, 4)
                    elif self.mutation_combo_box.itemText(self.mutation_combo_box.currentIndex()) == "Wszystkie metody mutacji losowo":
                        x = random.randint(0, 4)
                    elif self.mutation_combo_box.itemText(self.mutation_combo_box.currentIndex()) == "Wszystkie metody mutacji na zmianę":
                        x = self.epoch % 5
                    elif self.mutation_combo_box.itemText(self.mutation_combo_box.currentIndex()) == "Tylko mutacja przez wymieszanie przedziału":
                        x = 0
                    elif self.mutation_combo_box.itemText(self.mutation_combo_box.currentIndex()) == "Tylko mutacja przez zamianę pary miast":
                        x = 1
                    elif self.mutation_combo_box.itemText(self.mutation_combo_box.currentIndex()) == "Tylko mutacja przez inwersję i przesunięcie przedziału":
                        x = 2
                    elif self.mutation_combo_box.itemText(self.mutation_combo_box.currentIndex()) == "Tylko mutacja przez przestawienie jednego miasta":
                        x = 3 
                    elif self.mutation_combo_box.itemText(self.mutation_combo_box.currentIndex()) == "Tylko mutacja przez inwersję przedziału":
                        x = 4
                        
                    if x == 0: # Losowy przedział
                        mid_part = self.Potomostwo[i][index1:index2]  
                        random.shuffle(mid_part)  
                        self.Potomostwo[i][index1:index2] = mid_part                           
                                                                
                    elif x == 1: # Mutacja przez zamianę (na końcu)
                        self.Potomostwo[i][index1], self.Potomostwo[i][index2] = self.Potomostwo[i][index2], self.Potomostwo[i][index1]

                    elif x == 2: # Mutacja przez inwersję i przesunięcie przedziału
                        sublist_to_reverse = self.Potomostwo[i][index1:index2]
                        sublist_reversed = sublist_to_reverse[::-1]
                        shift_amount = random.randint(-(index1), len(self.Potomostwo[i]) - index2 - 1)

                        for star in sublist_reversed:
                            self.Potomostwo[i].remove(star)

                        self.Potomostwo[i][shift_amount:shift_amount] = sublist_reversed
                        
                    elif x == 3: # Mutacja przez wstawienie
                        city = self.Potomostwo[i].pop(index1)  
                        self.Potomostwo[i].insert(index2, city)
                        
                    elif x == 4: # Inwersja przedziału 
                        self.Potomostwo[i][index1:index2] = self.Potomostwo[i][index1:index2][::-1]

        
    def Sukcesja(self):
        if self.holistic_succession == True:
            self.Populacja.clear()
        self.Populacja.extend(self.Potomostwo)      
        self.Przystosowanie()
        self.Sortuj_Populacje()
        
        if self.holistic_succession == False: 
            self.Populacja = self.Populacja[:self.population_size]
            
    def Nowa_Kolonia(self):
        self.Kolonia.clear()
        for _ in range(self.num_ants):
            start = random.randint(0, len(self.Miasta) - 1)
            ant = self.Ant(self.Miasta[start], start)
            ant.unvisited_cities = [miasto for miasto in self.Miasta if miasto not in ant.visited_cities]
            ant.unvisited_cities_id = [id for id in range(len(self.Miasta)) if id not in ant.visited_cities_id]
            self.Kolonia.append(ant)
            
    def Reset_Feromonowy(self):
        self.pheromone_matrix = np.full((self.cities_number, self.cities_number), self.pheromone_amount)
   
    # Metoda Algorytmu Mrówkowego
    def Wybor_Tras_Mrowek(self):
        for _ in range(self.cities_number):
            for ant in self.Kolonia:
                self.Prawdopodobienstwa = []
                if ant.unvisited_cities_id:
                    for city_id in ant.unvisited_cities_id:
                        pheromone = self.pheromone_matrix[ant.current_city_id][city_id]
                        distance = self.ant_distances[ant.current_city_id][city_id]
                        probability = (pheromone**self.alpha) * ((1 / distance)**self.beta)
                        self.Prawdopodobienstwa.append([city_id, probability])
                        
                    total_prob = sum(prob for _, prob in self.Prawdopodobienstwa)
                    self.Prawdopodobienstwa = [[city_id, prob / total_prob] for city_id, prob in self.Prawdopodobienstwa]
                    # Korekta spowodowana koniecznością kompensacji błędów numerycznych
                    self.Prawdopodobienstwa[-1][1] += 1 - sum(prob for _, prob in self.Prawdopodobienstwa)
                    rand = random.random()
                    cumulative_prob = 0

                    for city_id, prob in self.Prawdopodobienstwa:
                        cumulative_prob += prob
                        if rand <= cumulative_prob:
                            if self.local_ph_update:
                                pheromone_delta = self.Q / (self.ant_distances[ant.current_city_id][city_id]*self.cities_number)
                                self.pheromone_matrix[ant.current_city_id][city_id] += pheromone_delta
                                self.pheromone_matrix[city_id][ant.current_city_id] += pheromone_delta
                            ant.visited_cities.append(self.Miasta[city_id])
                            ant.visited_cities_id.append(city_id)
                            ant.unvisited_cities.remove(self.Miasta[city_id])
                            ant.unvisited_cities_id.remove(city_id)
                            ant.current_city = self.Miasta[city_id]
                            ant.current_city_id = city_id
                            break
        self.Ocena_Kolonii()
        self.Parowanie_Feromonow()
        if not self.local_ph_update:
            self.Globalna_Aktualizacja_Feromonow()                                 
  
                    
    def Ocena_Kolonii(self):
        for ant in self.Kolonia:
            ant.full_route_length = sum(self.Dystans(ant.visited_cities[a-1],ant.visited_cities[a]) for a in range(self.cities_number))
        self.Kolonia.sort(key=lambda x: x.full_route_length)
        
    def Globalna_Aktualizacja_Feromonow(self):
        if self.queen:
            pheromone_delta = self.Q / self.Kolonia[0].full_route_length
            for idx in range(len(self.Kolonia[0].visited_cities_id)):
                self.pheromone_matrix[self.Kolonia[0].visited_cities_id[idx-1]][self.Kolonia[0].visited_cities_id[idx]] += pheromone_delta
                self.pheromone_matrix[self.Kolonia[0].visited_cities_id[idx]][self.Kolonia[0].visited_cities_id[idx-1]] += pheromone_delta             
        else:
            for ant in self.Kolonia:
                pheromone_delta = self.Q / ant.full_route_length
                for idx in range(len(ant.visited_cities_id)):
                    self.pheromone_matrix[ant.visited_cities_id[idx-1]][ant.visited_cities_id[idx]] += pheromone_delta
                    self.pheromone_matrix[ant.visited_cities_id[idx]][ant.visited_cities_id[idx-1]] += pheromone_delta             
    
    def Parowanie_Feromonow(self):
        self.pheromone_matrix *= (1 - self.rho)
         
    # Metoda dla symulowanego wyżarzania analogiczna względem mutacji
    def Modyfikacja(self):
        index1, index2 = random.sample(range(0, (len(self.new_sa)-1)), 2)
 
        if index1 > index2:
            index1, index2 = index2, index1

        if self.modification_combo_box.itemText(self.modification_combo_box.currentIndex()) == "Metoda modyfikacji zależna od temperatury":   
            if self.temperature >= 0.9 * self.initial_temperature:
                x = random.randint(0, 4) # Losowy przedział
            elif self.temperature >= 0.75 * self.initial_temperature:
                x = random.randint(1, 4) # Mutacja przez zamianę 
            elif self.temperature >= 0.55 * self.initial_temperature:
                x = random.randint(2, 4) # Mutacja przez wstawienie               
            elif self.temperature >= 0.3 * self.initial_temperature:
                x = random.randint(3, 4) # Inwersja przedziału               
            elif self.temperature >= 0:
                x = 4 # Mutacja przez inwersję i przesunięcie przedziału    
        elif self.modification_combo_box.itemText(self.modification_combo_box.currentIndex()) == "Wszystkie metody modyfikacji losowo":
            x = random.randint(0, 4)
        elif self.modification_combo_box.itemText(self.modification_combo_box.currentIndex()) == "Wszystkie metody modyfikacji na zmianę":
            x = self.epoch % 5
        elif self.modification_combo_box.itemText(self.modification_combo_box.currentIndex()) == "Tylko modyfikacja przez wymieszanie przedziału":
            x = 0
        elif self.modification_combo_box.itemText(self.modification_combo_box.currentIndex()) == "Tylko modyfikacja przez zamianę pary miast":
            x = 1
        elif self.modification_combo_box.itemText(self.modification_combo_box.currentIndex()) == "Tylko modyfikacja przez przestawienie jednego miasta":
            x = 2 
        elif self.modification_combo_box.itemText(self.modification_combo_box.currentIndex()) == "Tylko modyfikacja przez inwersję przedziału":
            x = 3
        elif self.modification_combo_box.itemText(self.modification_combo_box.currentIndex()) == "Tylko modyfikacja przez inwersję i przesunięcie przedziału":
            x = 4
            
        if x == 0: # Losowy przedział
            mid_part = self.new_sa[index1:index2]  
            random.shuffle(mid_part)  
            self.new_sa[index1:index2] = mid_part                           
                                                    
        elif x == 1: # Mutacja przez zamianę
            self.new_sa[index1], self.new_sa[index2] = self.new_sa[index2], self.new_sa[index1]
            
        elif x == 2: # Mutacja przez wstawienie
            city = self.new_sa.pop(index1)  
            self.new_sa.insert(index2, city)
            
        elif x == 3: # Inwersja przedziału 
            self.new_sa[index1:index2] = self.new_sa[index1:index2][::-1]   
              
        elif x == 4: # Mutacja przez inwersję i przesunięcie przedziału
            sublist_to_reverse = self.new_sa[index1:index2]
            sublist_reversed = sublist_to_reverse[::-1]
            shift_amount = random.randint(-(index1), len(self.new_sa) - index2 - 1)

            for star in sublist_reversed:
                self.new_sa.remove(star)

            self.new_sa[shift_amount:shift_amount] = sublist_reversed                             
            
    def Srednia_Genetyczna(self):
        self.average_distance = sum(self.Oceny) / len(self.Oceny)

    def Srednia_Mrowek(self):
        self.average_distance = sum(ant.full_route_length for ant in self.Kolonia) / len(self.Kolonia)
    
    def Zapis(self):
        self.folder_title = f"{self.cities_number}_miast"
        self.fig_title = "IMG"
        self.file_title = "XYZ"
        self.fig_title += f"_{self.cities_number}miast"
        self.file_title += f"_{self.cities_number}miast"
        self.fig_title += f"_{self.epoch}epok"
        self.file_title += f"_{self.epoch}epok"
        self.fig_title += f"_D{round(self.best_distance,2)}"
        self.file_title += f"_D{round(self.best_distance,2)}"
        expression = self.zfunction_combo_box.itemText(self.zfunction_combo_box.currentIndex())
        if expression == "Brak (Problem 2D)":
            self.function = "_none2D"
            self.fig_title += "_none2D"
            self.file_title += "_none2D"
        elif expression == "Brak (Kryterium euklidesowe 3D)":
            self.function = "_none3D"
            self.fig_title += "_none3D"
            self.file_title += "_none3D"
        elif expression == "Logarytm naturalny":
            self.function = "_ln"
            self.fig_title += "_ln"
            self.file_title += "_ln"
        elif expression == "Pierwiastek":
            self.function = "_root"
            self.fig_title += "root"
            self.file_title += "root"
        elif expression == "Liniowa":
            self.function = "_linear"
            self.fig_title += f"_linear"
            self.file_title += f"_linear"
        elif expression == "Kwadrat":
            self.function = "_square"
            self.fig_title += f"_square"
            self.file_title += f"_square"
        elif expression == "Sześcian":
            self.function = "_cube"
            self.fig_title += f"_cube"
            self.file_title += f"_cube"
        self.folder_title += self.function
        if self.genetic_button.isChecked():
            self.folder2_title = f"Genetyczny"
            self.fig_title += f"_Genetyczny"
            self.file_title += f"_Genetyczny"
            if self.random_init_button.isChecked():
                self.folder2_title += "_RI"
                self.fig_title += "_RI"
                self.file_title += "_RI"
            elif self.nn_init_button.isChecked():
                self.folder2_title += "_NN"
                self.fig_title += "_NN"
                self.file_title += "_NN"
            if self.mutation_combo_box.itemText(self.mutation_combo_box.currentIndex()) == "Mutacja dostosowana do stopnia zaawansowania rozwiązania":
                self.folder2_title += f"_Milestone"
                self.fig_title += f"_Milestone"
                self.file_title += f"_Milestone"
            else:
                self.folder2_title += f"_Mrand"
                self.fig_title += f"_Mrand"
                self.file_title += f"_Mrand"                
            self.fig_title += f"_K{self.crossover_chance}_M{self.mutation_chance}_P{self.population_size}+({self.offspring_size})"
            self.file_title += f"_K{self.crossover_chance}_M{self.mutation_chance}_P{self.population_size}+({self.offspring_size})"
            self.folder2_title += f"_K{self.crossover_chance}_M{self.mutation_chance}_P{self.population_size}+({self.offspring_size})"
            
        elif self.swarm_button.isChecked():
            self.folder2_title = f"Mrowkowy"
            self.fig_title += f"_Mrowkowy"
            self.file_title += f"_Mrowkowy"
            if self.queen:
                self.folder2_title += "_Queen"
                self.fig_title += f"_Queen"
                self.file_title += f"_Queen"
            elif self.local_ph_update:
                self.folder2_title += "_Localph"
                self.fig_title += f"_Localph"
                self.file_title += f"_Localph"
            else:
                self.folder2_title += "_Globalph"
                self.fig_title += f"_Globalph"
                self.file_title += f"_Globalph"
                
            self.folder2_title += f"_{self.num_ants}ants_alpha{self.alpha}_beta{self.beta}_rho{self.alpha}_Q{self.Q}"
            self.fig_title += f"_{self.num_ants}ants_alpha{self.alpha}_beta{self.beta}_rho{self.alpha}_Q{self.Q}"
            self.file_title += f"_{self.num_ants}ants_alpha{self.alpha}_beta{self.beta}_rho{self.alpha}_Q{self.Q}"

        elif self.annealing_button.isChecked():
            self.folder2_title = f"Wyzarzanie"
            self.fig_title += f"_Wyzarzanie"
            self.file_title += f"_Wyzarzanie"           
            if self.random_init_button.isChecked():
                self.folder2_title += "_RI"
                self.fig_title += "_RI"
                self.file_title += "_RI"
            elif self.nn_init_button.isChecked():
                self.folder2_title += "_NN"
                self.fig_title += "_NN"
                self.file_title += "_NN"
            if self.modification_combo_box.itemText(self.modification_combo_box.currentIndex()) == "Metoda modyfikacji zależna od temperatury":
                self.folder2_title += f"_Milestone"
                self.fig_title += f"_Milestone"
                self.file_title += f"_Milestone"
            else:
                self.folder2_title += f"_Mrand"
                self.fig_title += f"_Mrand"
                self.file_title += f"_Mrand"
            self.fig_title += f"_IT{self.initial_temperature}+cr({self.cooling_rate})"
            self.file_title += f"_IT{self.initial_temperature}+cr({self.cooling_rate})"
            self.folder2_title += f"_IT{self.initial_temperature}+cr({self.cooling_rate})"
        
        self.fig_title += ".png"
        self.file_title += ".txt"
        
        if not os.path.exists(f"Zapisy/{self.folder_title}/{self.folder2_title}"):
            os.makedirs(f"Zapisy/{self.folder_title}/{self.folder2_title}")
        
        self.fig.savefig(f"Zapisy/{self.folder_title}/{self.folder2_title}/{self.fig_title}")  
        img = Image.frombytes('RGB', self.fig.canvas.get_width_height(), self.fig.canvas.tostring_rgb()) 
        cropped_img3D = img.crop((20, 100, 495, 590))
        cropped_img3D.save(f"Zapisy/{self.folder_title}/{self.folder2_title}/3D_{self.fig_title}")
        cropped_img2D = img.crop((490, 100, 1050, 590))
        cropped_img2D.save(f"Zapisy/{self.folder_title}/{self.folder2_title}/2D_{self.fig_title}")

        with open(f"Zapisy/{self.folder_title}/{self.folder2_title}/{self.file_title}", "w") as file:
            for miasto in self.best:
                file.write(f"{miasto[0]} {miasto[1]} {miasto[2]}\n")

                
    def X_iteracji(self):
        for _ in range(self.iterations_box.value()):
            if self.genetic_button.isChecked():
                self.Main_Genetyczny()
            elif self.swarm_button.isChecked():
                self.Main_Mrowkowy()
            elif self.annealing_button.isChecked():
                self.Main_Wyzarzanie()

    def Rysuj_Genetyczny(self):
            
        self.current_epoch_info = self.ax.text2D(0.02, 1, '', transform=self.ax.transAxes)
        self.best_epoch_info = self.ax.text2D(0.02, 0.95, '', transform=self.ax.transAxes)
        self.best_path_length_info = self.ax.text2D(0.55, 1, '', transform=self.ax.transAxes)
        self.average_length_info = self.ax.text2D(0.55, 0.95, '', transform=self.ax.transAxes)
        self.current_length_info = self.ax.text2D(0.55, 0.95, '', transform=self.ax.transAxes)
    
            
        if (self.task2D or self.rainbow3D):
            c = np.arange(1, len(self.Miasta))
            norm = mpl.colors.Normalize(vmin=c.min(), vmax=c.max())
            self.cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.gist_rainbow)
            self.cmap.set_array([])
        
            x, y, z = zip(*self.best)
            self.Punkty = self.ax.scatter(x, y, z, marker="o", c=self.cmap.to_rgba(range(len(self.best))))
            self.Linie = [self.ax.plot3D([self.best[i-1][0], self.best[i][0]],
                                        [self.best[i-1][1], self.best[i][1]],
                                        [self.best[i-1][2], self.best[i][2]],
                                        c=self.cmap.to_rgba(i))[0] for i in range(len(self.best))]
        else:
            z_values_span = []
            for i in range(len(self.best)):
                start_z = self.best[i-1][2]
                end_z = self.best[i][2]
                z_span = abs(start_z - end_z)
                z_values_span.append(z_span)            
            # Przekształcam listę wartości średnich z na tablicę NumPy
            z_values_span = np.array(z_values_span)
            
            norm = Normalize(vmin=min(z_values_span), vmax=max(z_values_span))
            normalized_values = norm(z_values_span)
        
            x, y, z = zip(*self.best)
            c = np.array(self.best)[:, 2]  # Przykład: wybieramy współrzędną z punktu z

            self.Punkty = self.ax.scatter(x, y, z, marker="o", c=c, cmap=self.cmap)
            self.Linie = [self.ax.plot3D([self.best[i-1][0], self.best[i][0]],
                                        [self.best[i-1][1], self.best[i][1]],
                                        [self.best[i-1][2], self.best[i][2]],
                                        c=self.cmap(normalized_values[i]))[0] for i in range(len(self.best))]


        
        # Resetujemy dane dla wykresu 2D
        self.epochs = []
        self.path_lengths = []
        self.average_path_lengths = []
        self.line, = self.ax2.plot([], [], color='cyan', label='Przystosowanie najlepszego osobnika')  
        self.average_line, = self.ax2.plot([], [], color=(0.5, 0.1, 0.7), linestyle='--', label='Średnie przystosowanie w populacji')  
        self.ax2.legend(loc='upper right')
        
    def Rysuj_Mrowkowy(self):
        self.current_epoch_info = self.ax.text2D(0.02, 1, '', transform=self.ax.transAxes)
        self.best_epoch_info = self.ax.text2D(0.02, 0.95, '', transform=self.ax.transAxes)
        self.best_path_length_info = self.ax.text2D(0.55, 1, '', transform=self.ax.transAxes)
        self.average_length_info = self.ax.text2D(0.55, 0.95, '', transform=self.ax.transAxes)
        self.current_length_info = self.ax.text2D(0.55, 0.95, '', transform=self.ax.transAxes)
        
        if (self.task2D or self.rainbow3D):
            c = np.arange(1, len(self.Miasta))
            norm = mpl.colors.Normalize(vmin=c.min(), vmax=c.max())
            self.cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.gist_rainbow)
            self.cmap.set_array([])
        
            x, y, z = zip(*self.best)
            self.Punkty = self.ax.scatter(x, y, z, marker="o", c=self.cmap.to_rgba(range(len(self.best))))
            self.Linie = [self.ax.plot3D([self.best[i-1][0], self.best[i][0]],
                                        [self.best[i-1][1], self.best[i][1]],
                                        [self.best[i-1][2], self.best[i][2]],
                                        c=self.cmap.to_rgba(i))[0] for i in range(len(self.best))]        
        else:
            z_values_span = []
            for i in range(len(self.best)):
                start_z = self.best[i-1][2]
                end_z = self.best[i][2]
                z_span = abs(start_z - end_z)
                z_values_span.append(z_span)
                
            # Przekształcam listę wartości średnich z na tablicę NumPy
            z_values_span = np.array(z_values_span)
            
            norm = Normalize(vmin=min(z_values_span), vmax=max(z_values_span))
            normalized_values = norm(z_values_span)
        
            x, y, z = zip(*self.best)
            c = np.array(self.best)[:, 2]  # Przykład: wybieramy współrzędną z punktu z

            self.Punkty = self.ax.scatter(x, y, z, marker="o", c=c, cmap=self.cmap)
            self.Linie = [self.ax.plot3D([self.best[i-1][0], self.best[i][0]],
                                        [self.best[i-1][1], self.best[i][1]],
                                        [self.best[i-1][2], self.best[i][2]],
                                        c=self.cmap(normalized_values[i]))[0] for i in range(len(self.best))]
            
        # Resetujemy dane dla wykresu 2D
        self.epochs = []
        self.path_lengths = []
        self.average_path_lengths = []
        self.line, = self.ax2.plot([], [], color='#0099FF', label='Ocena najlepszej mrówki')  
        self.average_line, = self.ax2.plot([], [], color='#00FF66', linestyle='--', label='Średnia ocena mrówek')  
        self.ax2.legend(loc='upper right')
        
    def Rysuj_Wyzarzanie(self):
        self.current_epoch_info = self.ax.text2D(0.02, 1, '', transform=self.ax.transAxes)
        self.best_epoch_info = self.ax.text2D(0.02, 0.95, '', transform=self.ax.transAxes)
        self.risk_counter_info = self.ax.text2D(0.02, 1.05, '', transform=self.ax.transAxes)
        self.current_temperature_info = self.ax.text2D(0.55, 1.05, '', transform=self.ax.transAxes)
        self.best_path_length_info = self.ax.text2D(0.55, 1, '', transform=self.ax.transAxes)
        self.current_length_info = self.ax.text2D(0.55, 0.95, '', transform=self.ax.transAxes)
        
        if (self.task2D or self.rainbow3D):
            c = np.arange(1, len(self.Miasta))
            norm = mpl.colors.Normalize(vmin=c.min(), vmax=c.max())
            self.cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.gist_rainbow)
            self.cmap.set_array([])
        
            x, y, z = zip(*self.best)
            self.Punkty = self.ax.scatter(x, y, z, marker="o", c=self.cmap.to_rgba(range(len(self.best))))
            self.Linie = [self.ax.plot3D([self.best[i-1][0], self.best[i][0]],
                                        [self.best[i-1][1], self.best[i][1]],
                                        [self.best[i-1][2], self.best[i][2]],
                                        c=self.cmap.to_rgba(i))[0] for i in range(len(self.best))]       
        else:
            z_values_span = []
            for i in range(len(self.best)):
                start_z = self.best[i-1][2]
                end_z = self.best[i][2]
                z_span = abs(start_z - end_z)
                z_values_span.append(z_span)
                
            # Przekształcam listę wartości średnich z na tablicę NumPy
            z_values_span = np.array(z_values_span)
            
            norm = Normalize(vmin=min(z_values_span), vmax=max(z_values_span))
            normalized_values = norm(z_values_span)
        
            x, y, z = zip(*self.best)
            c = np.array(self.best)[:, 2]  # Przykład: wybieramy współrzędną z punktu z

            self.Punkty = self.ax.scatter(x, y, z, marker="o", c=c, cmap=self.cmap)
            self.Linie = [self.ax.plot3D([self.best[i-1][0], self.best[i][0]],
                                        [self.best[i-1][1], self.best[i][1]],
                                        [self.best[i-1][2], self.best[i][2]],
                                        c=self.cmap(normalized_values[i]))[0] for i in range(len(self.best))]

        # Resetujemy dane dla wykresu 2D
        self.epochs = []
        self.path_lengths = []
        self.current_path_lengths = []
        self.new_path_lengths = []
        self.line, = self.ax2.plot([], [], color='magenta', label='Ocena najlepszego rozwiązania')  
        self.current_line, = self.ax2.plot([], [], color='blue', linestyle='--', label='Ocena bieżącego rozwiązania')
        self.new_line, = self.ax2.plot([], [], color='orange', linestyle='-.', label='Ocena nowego rozwiązania')    
        self.ax2.legend(loc='upper right')
        

    def Main_Genetyczny(self):
        self.epoch += 1
        
        self.Przystosowanie()
        self.Selekcja()  
        self.Krzyzowanie()
        self.Mutacja()
        self.Sukcesja() 
        
        # Aktualizacja najlepszego osobnika
        if self.Oceny[0] < self.best_distance:
            self.best_distance = self.Oceny[0]
            self.birth_of_the_best = self.epoch
            self.best = self.Populacja[0].copy()
            
            elapsed_time = time.time() - self.start_time
            hours = int(round(elapsed_time // 3600,2))
            minutes = int(round((elapsed_time % 3600) // 60,2))
            seconds = int(round(elapsed_time % 60,2))
            print(f"Dystans {self.best_distance} - A upłynęło {hours} godzin {minutes} minut i {seconds} sekund(y)")     
            
        
        x, y, z = zip(*self.best)
        self.Punkty._offsets3d = x, y, z
        
        if not (self.task2D or self.rainbow3D):
            z_values_span = []
            for i in range(len(self.best)):
                start_z = self.best[i-1][2]
                end_z = self.best[i][2]
                z_span = abs(start_z - end_z)
                z_values_span.append(z_span)
                    
            #norm = Normalize(vmin=min(z_values_span), vmax=max(z_values_span))
            norm = Normalize(vmin=0, vmax= abs(self.z_range[1] - self.z_range[0])/2)
            normalized_values = norm(z_values_span)
            
            # for label in self.ax.texts:
            #     label.remove()
        
        for i, linia in enumerate(self.Linie):
            linia.set_data_3d([self.best[i-1][0], self.best[i][0]],
                              [self.best[i-1][1], self.best[i][1]],
                              [self.best[i-1][2], self.best[i][2]])
            dlugosc_linii = self.Dystans(self.best[i-1], self.best[i])
            
            # label_x = (self.best[i-1][0] + self.best[i][0]) / 2
            # label_y = (self.best[i-1][1] + self.best[i][1]) / 2
            # label_z = (self.best[i-1][2] + self.best[i][2]) / 2
            # label_text = '{:.1f}'.format(dlugosc_linii)
            # self.ax.text(label_x, label_y, label_z, label_text, fontsize=10, color = "blue")
            if not (self.task2D or self.rainbow3D):
                linia.set_color(self.cmap(normalized_values[i]))
       
        if not (self.task2D or self.rainbow3D):    
            self.Punkty.set_array(np.array(z_values_span))
            self.Punkty.set_cmap(self.cmap)
            self.Punkty.set_norm(norm)

        self.current_epoch_info.set_text('Aktualna epoka: {}'.format(self.epoch))
        self.best_epoch_info.set_text('Ostatnia poprawa: {}'.format(self.birth_of_the_best))
        self.Srednia_Genetyczna()
        self.best_path_length_info.set_text('Rekord populacyjny: {:.2f}'.format(self.best_distance))
        self.average_length_info.set_text('Średnia populacyjna: {:.2f}'.format(self.average_distance))
        
        # Aktualizowanie wykresu 2D
        self.epochs.append(self.epoch)
        self.path_lengths.append(self.best_distance)
        self.average_path_lengths.append(self.average_distance)
        self.line.set_data(self.epochs, self.path_lengths)
        self.average_line.set_data(self.epochs, self.average_path_lengths)
        self.ax2.relim()
        self.ax2.autoscale_view()
        
        if self.autosave and (self.epoch)!= 0 and (self.epoch) % (self.autosave_period) == 0:
            self.Zapis()
        if self.epoch == 50000: 
            self.Zapis()  
            self.Zatrzymanie_Animacji()
        
    def Main_Mrowkowy(self):
        self.epoch += 1
        self.Nowa_Kolonia()
        self.Wybor_Tras_Mrowek()

        # Jeśli od pewnego czasu nie udało się uzyskać lepszego rozwiązania to resetujemy macierz feromonową
        if (self.epoch - self.birth_of_the_best)!= 0 and (self.epoch - self.birth_of_the_best) % (self.pheromone_reset_threshold) == 0:
            self.Reset_Feromonowy()
            
        # Aktualizacja najlepszego osobnika
        if self.Kolonia[0].full_route_length < self.best_distance:
            self.best_distance = self.Kolonia[0].full_route_length
            self.birth_of_the_best = self.epoch
            self.best = self.Kolonia[0].visited_cities.copy()
            
            elapsed_time = time.time() - self.start_time
            hours = int(round(elapsed_time // 3600,2))
            minutes = int(round((elapsed_time % 3600) // 60,2))
            seconds = int(round(elapsed_time % 60,2))
            print(f"Dystans {self.best_distance} - A upłynęło {hours} godzin {minutes} minut i {seconds} sekund(y)")     
            
        x, y, z = zip(*self.best)
        self.Punkty._offsets3d = x, y, z
        if not (self.task2D or self.rainbow3D):
            z_values_span = []
            for i in range(len(self.best)):
                start_z = self.best[i-1][2]
                end_z = self.best[i][2]
                z_span = abs(start_z - end_z)
                z_values_span.append(z_span)
                
            norm = Normalize(vmin=0, vmax= abs(self.z_range[1] - self.z_range[0])/2)
            normalized_values = norm(z_values_span)
             
        for i, linia in enumerate(self.Linie):
            linia.set_data_3d([self.best[i-1][0], self.best[i][0]],
                              [self.best[i-1][1], self.best[i][1]],
                              [self.best[i-1][2], self.best[i][2]])
            if not (self.task2D or self.rainbow3D):
                linia.set_color(self.cmap(normalized_values[i]))
        if not (self.task2D or self.rainbow3D):    
            self.Punkty.set_array(np.array(z_values_span))
            self.Punkty.set_cmap(self.cmap)
            self.Punkty.set_norm(norm)

        self.current_epoch_info.set_text('Aktualna epoka: {}'.format(self.epoch))
        self.best_epoch_info.set_text('Ostatnia poprawa: {}'.format(self.birth_of_the_best))
        self.Srednia_Mrowek()
        self.best_path_length_info.set_text('Rekord wśród kolonii: {:.2f}'.format(self.best_distance))
        self.average_length_info.set_text('Średnia bieżącej kolonii: {:.2f}'.format(self.average_distance))
        
        # Aktualizowanie wykresu 2D
        self.epochs.append(self.epoch)
        self.path_lengths.append(self.best_distance)
        self.average_path_lengths.append(self.average_distance)
        self.line.set_data(self.epochs, self.path_lengths)
        self.average_line.set_data(self.epochs, self.average_path_lengths)
        self.ax2.relim()
        self.ax2.autoscale_view()
        
        if self.autosave and (self.epoch)!= 0 and (self.epoch) % (self.autosave_period) == 0:
            self.Zapis()
        if self.epoch == 50000: 
            self.Zapis()  
            self.Zatrzymanie_Animacji()
        
    def Main_Wyzarzanie(self):
        self.epoch += 1
        self.Modyfikacja()
        self.new_sa_distance = sum(self.Dystans(self.new_sa[k-1],self.new_sa[k]) for k in range(len(self.new_sa)))
        
        delta_distance = self.new_sa_distance - self.current_sa_distance
        if (delta_distance < 0):
            self.current_sa, self.current_sa_distance = self.new_sa.copy(), self.new_sa_distance
        elif (random.random() < m.exp(-delta_distance / self.temperature)):
            self.risk_counter += 1
            self.current_sa, self.current_sa_distance = self.new_sa.copy(), self.new_sa_distance
        else:
            self.new_sa = self.current_sa.copy()
            
        # Schłodzenie temperatury
        self.temperature *= 1 - self.cooling_rate
        
        # Aktualizacja najlepszego osobnika
        if self.current_sa_distance < self.best_distance:
            self.best_distance = self.current_sa_distance
            self.birth_of_the_best = self.epoch
            self.best = self.new_sa.copy()
            
            elapsed_time = time.time() - self.start_time
            hours = int(round(elapsed_time // 3600,2))
            minutes = int(round((elapsed_time % 3600) // 60,2))
            seconds = int(round(elapsed_time % 60,2))
            print(f"Dystans {self.best_distance} - A upłynęło {hours} godzin {minutes} minut i {seconds} sekund(y)")          
        
        x, y, z = zip(*self.best)
        self.Punkty._offsets3d = x, y, z
        if not (self.task2D or self.rainbow3D):
            z_values_span = []
            for i in range(len(self.best)):
                start_z = self.best[i-1][2]
                end_z = self.best[i][2]
                z_span = abs(start_z - end_z)
                z_values_span.append(z_span)
                    
            norm = Normalize(vmin=0, vmax= abs(self.z_range[1] - self.z_range[0])/2)
            normalized_values = norm(z_values_span)
        
        
        for i, linia in enumerate(self.Linie):
            linia.set_data_3d([self.best[i-1][0], self.best[i][0]],
                              [self.best[i-1][1], self.best[i][1]],
                              [self.best[i-1][2], self.best[i][2]]) 
            if not (self.task2D or self.rainbow3D):       
                linia.set_color(self.cmap(normalized_values[i]))
        if not (self.task2D or self.rainbow3D):    
            self.Punkty.set_array(np.array(z_values_span))
            self.Punkty.set_cmap(self.cmap)
            self.Punkty.set_norm(norm)

        self.current_epoch_info.set_text('Aktualna epoka: {}'.format(self.epoch))
        self.best_epoch_info.set_text('Ostatnia poprawa: {}'.format(self.birth_of_the_best))
        self.risk_counter_info.set_text('Podjęte ryzyka: {}'.format(self.risk_counter))
        self.current_temperature_info.set_text('Aktualna temperatura: {}'.format(round(self.temperature,2)))
        self.best_path_length_info.set_text('Rekordowe rozwiązanie: {:.2f}'.format(self.best_distance))
        self.current_length_info.set_text('Bieżące rozwiązanie: {:.2f}'.format(self.current_sa_distance))
        
        # Aktualizowanie wykresu 2D
        self.epochs.append(self.epoch)
        self.path_lengths.append(self.best_distance)
        self.new_path_lengths.append(self.new_sa_distance)
        self.current_path_lengths.append(self.current_sa_distance)
        self.line.set_data(self.epochs, self.path_lengths)
        self.current_line.set_data(self.epochs, self.current_path_lengths)
        self.new_line.set_data(self.epochs, self.new_path_lengths)
        self.ax2.relim()
        self.ax2.autoscale_view()
        
        # Jeśli od pewnego czasu nie udało się uzyskać lepszego oszacowania, to znowu ustawiamy najlepsze rozwiązanie jako bieżące
        if (self.epoch - self.birth_of_the_best)!= 0 and (self.epoch - self.birth_of_the_best) % (len(self.Miasta)) == 0:
            self.current_sa, self.current_sa_distance = self.best.copy(), self.best_distance
            
        if self.autosave and (self.epoch)!= 0 and (self.epoch) % (self.autosave_period) == 0:
            self.Zapis()
        if self.epoch == 50000: 
            self.Zapis()  
            self.Zatrzymanie_Animacji()



aplikacja = QApplication(sys.argv)
okno = MyWindow()
okno.show()
sys.exit(aplikacja.exec_())

