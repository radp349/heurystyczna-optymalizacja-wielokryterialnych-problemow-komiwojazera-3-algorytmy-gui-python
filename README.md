# Instrukcja dla Użytkownika

Aplikacja Heurystyczne_TSP_3D.py zawiera algorytm genetyczny, mrówkowy oraz symulowane wyżarzanie, dostosowane do rozwiązywania wielokryterialnych, symetrycznych, statycznych, dwuwymiarowych oraz trójwymiarowych, liniowych i nieliniowych problemów komiwojażera. Aby ją uruchomić, konieczne jest spełnienie podanych poniżej wymagań:

-   Zainstalowany interpreter języka Python w wersji 3.8 lub wyższej,
    króty musi być dodany do zmiennej środowiskowej PATH.

-   System operacyjny: Windows lub Linux

-   Zainstalowany i skonfigurowany instalator pakietów PIP, a także
    moduły: *sys*, *PyQt5*, *random*, *math*, *PIL*, *matplotlib*, *os*
    oraz *numpy*.

-   Zainstalowane środowisko programistyczne obsługujące język Python,
    takie jak dajmy na to *Visual Studio Code* bądź *PyCharm*. W
    przypadku tego pierwszego zaleca się dodatkowo zainstalować
    rozszerzenie do obsługi składni języka Python. Następnie należy
    wybrać wersję interpretera, która będzie używana przy uruchamiania
    skryptów.

Poniżej został przedstawiony zrzut ekranu z programu,
podczas optymalizacji algorytmem genetycznym. Aplikacja została
wyposażona w interaktywny graficzny interfejs użytkownika, składający
się z: pól tekstowych, checkbox'ów, przełączników, etykiet i list
rozwijanych. Jednocześnie wyświetlane są tylko te opcje, które dotyczą
aktualnie wybranego algorytmu. Aby skorzystać z programu należy wybrać
opcję *Wczytaj* i za pomocą przycisku *Przeglądaj* wybrać ścieżkę do
listy punktów (X,Y,Z) wypisanych w pliku o rozszerzeniu .txt, przy czym
każda współrzędna musi być oddzielona spacją, a każdy punkt musi
znajdować się w nowej linii. Drugą alternatywą jest wylosowanie własnego
problemu, poprzez zdefiniowanie liczby miast oraz wymiarów siatki na
której zostaną one umieszczone.

![Menu główne algorytmu genetycznego](Image/menu.png)


Z racji koncepcji pracy nie jest możliwe wygenerowanie chmury punktów,
choć takowa mogłaby zostać przetworzona gdyby została wczytana. Każdy
aktualnie przetwarzany problem, zarówno wczytany jak i wylosowany, można
zachować poprzez użycie przycisku *Zapisz Bieżące Rozwiązanie*.
Spowoduje to zapis najlepszego znalezionego rozwiązania jako listę
punktów do pliku .txt, a także połączonego i rozdzielonego stanu
wykresów do trzech oddzielnych plików graficznych. Domyślnie włączone
jest cykliczne automatyczne wykonywanie zapisów. Kolejną kwestią jest
wybór funkcji kosztu $Z$ trzeciego wymiaru. Wybór opcji *Brak (Problem 2D)*
spowoduje zastąpienie wartości współrzędnych Z wszystkich punktów
zerami, a także ustawi orientację wykresu 3D na widok z góry. W tym
konkretnym przypadku występuje też inna (tęczowa) mapa kolorów.

**Kluczowym aspektem nastaw jest wybór algorytmu optymalizacji:**

-   Algorytm genetyczny *(AG)*

    -   Do wyboru jest inicjalizacja losowa, co oznacza zupełnie
        chaotyczną populację początkową, bądź wstępna poprawa jakości
        rozwiązań dzięki wykorzystaniu algorytmu *Najbliższego Sąsiada*
        w charakterze optymalizacji lokalnej.

    -   Można wykorzystywać selekcję kołem ruletki (z zaimplementowanym
        skalowaniem) lub selekcję turniejową (turniej deterministyczny z
        prawdopodobieństwem 0.9 zwycięstwa lepiej przystosowanego
        osobnika), obie te metody na zmianę w parzystych i nieparzystych
        epokach, bądź każdorazowo wybór losowej metody selekcji.

    -   Dostępna jest zarówno sukcesja całościowa jak i częściowa przy
        tworzeniu populacji potomnej $P_{t+1}$.

    -   Można określić różne sensowne obliczeniowo nieujemne rozmiary
        populacji bazowej i potomnej.

    -   Analogicznie do selekcji, dostępne są 2 metody krzyżowania *CX*
        i *PMX* wykorzystywane indywidualnie, naprzemiennie lub losowo.
        Stosowne pole tekstowo umożliwia określenie szansy wystąpienia
        krzyżowania dla przypadkowo dobranej pracy osobników.

    -   Dopuszczalne jest określenie prawdopodobieństwa zajścia mutacji
        oraz wybór jej rodzaju. Wskazane są opcje wykorzystywania
        wszystkich metod losowo lub dostosowania ich użycia do stopnia
        zaawansowania rozwiązania.

-   Algorytm mrówkowy *(ACO)* 
    -   Należy wybrać liczebność grupy mrówek, dokonującej optymalizacji
        w każdej iteracji. Sugerowana jest liczba w przedziale 50 -150%
        liczby miast.

    -   Każda mrówka będzie w zależności od decyzji użytkownika
        zostawiać feromon lokalnie natychmiast po przejściu danego
        odcinka trasy albo globalnie po przejściu całości. W zależności
        ilość zostawionego feromonu będzie odwrotnie proporcjonalna do
        długości konkretnego fragmentu lub do długości całej trasy.

    -   W globalnej wersji pozostawiania feromonu istnieje możliwość aby
        robiła to tylko najlepsza mrówka z bieżącej grupy.

    -   Możliwy jest wybór wartości parametrów $\alpha$, $\beta$, $\rho$
        i $\gamma$ określających kolejno stochastyczną części procesu
        decyzyjnego mrówki, tempo parowania feromonów oraz skalowanie
        ich ilości pozostawianej przez jednostkę roju.

    -   Aby efektywnie korzystać a algorytmu mrówkowego, należy co jakiś
        czas wykonać reset całej macierzy feromonowej. Zadana liczba
        oznacza po ilu epokach bez poprawy rozwiązania nastąpi reset.

-   Symulowane Wyżarzanie *(SA)* 

    -   Tak jak w *AG* do wyboru jest inicjalizacja losowa bądź
        algorytmem *Najbliższego Sąsiada*

    -   Należy wskazać wartość początkową temperatury oraz współczynnik
        chłodzenia.

    -   Analogicznie do mutacji w *AG* zachodzi konieczność wyboru
        sposobu modyfikacji bieżącego rozwiązania. Dostępne możliwości
        są zawarte w menu kontekstowym.

| ![Menu algorytmu mrówkowego](Image/menuaco.png) | ![Menu symulowanego wyżarzania](Image/menusa.png) |
|------------------------------------------------|-------------------------------------------------|
| Menu algorytmu mrówkowego                      | Menu symulowanego wyżarzania                    |

Na dole każdego menu znajdują się przyciski wspólne dla wszystkich
trzech algorytmów.Pracę programu można w każdej chwili wstrzymać przez
naciśnięcie przycisku *Zatrzymaj* i ponownie uruchomić klikając *Wznów*.
Niezależnie od trwającej pauzy można zlecić zarówno wykonanie
konkretniej liczby iteracji jak i dokonanie zapisu. Ostatnimi elementami
są przycisk *Start* oraz *Zakończ*. Ten pierwszy zatwierdza wszystkie
wprowadzone nastawy, blokuje możliwość ich wprowadzania, uruchamia
proces optymalizacji, a także inicjuje animację. Ten drugi resetuje kod,
odblokowuje całe menu, ponadto wymazuje wszystkie wykresy. Dzięki temu
nie jest wymagane każdorazowe zamykanie okna oraz ponowna kompilacja
kodu.
